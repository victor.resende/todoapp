package br.prtt.pdm.todoapp.data

class DBSchema {
    object TaskTable{
        const val TABLENAME = "tasks"
        const val ID = "t_id"
        const val URGENT = "t_urgent"
        const val DONE = "t_done"
        const val DESCRIPTION = "t_description"
        const val TIMESTAMP = "t_timestamp"
        fun getCreateTableQuery(): String{
            return """
                CREATE TABLE IF NOT EXISTS $TABLENAME (
                    $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    $DESCRIPTION TEXT NOT NULL,
                    $URGENT INTEGER NOT NULL,
                    $DONE INTEGER NOT NULL,
                    $TIMESTAMP TEXT DEFAULT CURRENT_TIMESTAMP
                );
            """.trimIndent()

        }
    }
}