package br.prtt.pdm.todoapp.model

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable

class Tasks constructor(
        private var description: String = "",
        private var isUrgent: Boolean = false,
        private var isDone: Boolean = false
): Parcelable {
    private var id: Int = 0


    // DESCOBRI PRA QUE SERVE
    constructor(id: Int, description: String, isUrgent: Boolean, isDone: Boolean): this(description, isUrgent, isDone){
        this.id = id
    }

    fun getId(): Int {
        return this.id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getDescription(): String {
        return this.description
    }

    fun getIsUrgent(): Boolean {
        return this.isUrgent
    }

    fun getIsDone(): Boolean {
        return this.isDone
    }

    fun setDescription(description: String) {
        this.description = description
    }

    fun setIsUrgent(isUrgent: Boolean) {
        this.isUrgent = isUrgent
    }

    fun setIsDone(isDone: Boolean) {
        this.isDone = isDone
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false
        other as Tasks
        if (id != other.id) return false
        return true
    }

    override fun hashCode(): Int {
        return id
    }




    // -------- PARCELABLE

    @SuppressLint("NewApi")
    private constructor(parcel: Parcel) : this(""){
        this.id = parcel.readInt()
        this.description = parcel.readString()!!
        this.isUrgent = parcel.readBoolean()
        this.isDone = parcel.readBoolean()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(this.id)
        parcel.writeString(this.description)
        parcel.writeByte(if (this.isUrgent) 1 else 0)
        parcel.writeByte(if (this.isDone) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }



    companion object CREATOR : Parcelable.Creator<Tasks> {
        override fun createFromParcel(parcel: Parcel): Tasks {
            return Tasks(parcel)
        }

        override fun newArray(size: Int): Array<Tasks?> {
            return arrayOfNulls(size)
        }
    }


}