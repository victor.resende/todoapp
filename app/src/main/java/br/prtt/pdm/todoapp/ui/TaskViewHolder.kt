package br.prtt.pdm.todoapp.ui

import android.graphics.Color
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import br.prtt.pdm.todoapp.R
import br.prtt.pdm.todoapp.data.TaskDAOSingleton
import br.prtt.pdm.todoapp.model.Tasks


class TaskViewHolder(
    itemView: View,
    private val adapter: TaskListAdapter
    ) : RecyclerView.ViewHolder(itemView), View.OnLongClickListener {
    private val txtTextTask: TextView = itemView.findViewById(R.id.txtTextTask)
    private val imageUrgent: ImageView = itemView.findViewById(R.id.imageUrgent)
    private val checkBoxDone: CheckBox = itemView.findViewById(R.id.checkBoxDone)
    private lateinit var tempTask: Tasks
    init{
        itemView.setOnLongClickListener(this)
        this.checkBoxDone.setOnCheckedChangeListener{_,isChecked ->
            this.tempTask.setIsDone(isChecked)
           TaskDAOSingleton.update(itemView.context,this.tempTask)
        }
    }
    fun bind(task: Tasks){
        this.tempTask = task
        this.txtTextTask.text = this.tempTask.getDescription()
        this.checkBoxDone.isChecked = this.tempTask.getIsDone()
        if(tempTask.getIsUrgent()){
            imageUrgent.setBackgroundColor(Color.RED)
        }else{
            imageUrgent.setBackgroundColor(Color.GREEN)
        }


    }

    override fun onLongClick(v: View?): Boolean{
       this.adapter.getOnLongClickTaskListener()?.onClickTask(this.tempTask)
        this.adapter.lastClickedTask = this.tempTask
        return true
    }


}