package br.prtt.pdm.todoapp.ui


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import br.prtt.pdm.todoapp.R
import br.prtt.pdm.todoapp.data.TaskDAOSingleton
import br.prtt.pdm.todoapp.model.Tasks


class MainActivity : AppCompatActivity() {


    private lateinit var plainTextTask: EditText
    private lateinit var switch_toggle: SwitchCompat
    private lateinit var cbNotDone: CheckBox
    private lateinit var recyclerView: RecyclerView
    private lateinit var taskListAdapter: TaskListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.plainTextTask = findViewById(R.id.plainTextTask)
        this.switch_toggle = findViewById(R.id.switch_urgent)
        this.cbNotDone = findViewById(R.id.checkBoxNotDone)

        cbNotDone.setOnClickListener{
            if(cbNotDone.isChecked){
                this.taskListAdapter.filterNotDoneTasks(true)
            }else{
                this.taskListAdapter.filterNotDoneTasks(false)

            }
        }


        this.recyclerView = findViewById(R.id.rvTaskList)
        this.recyclerView.layoutManager = LinearLayoutManager(this)
        this.recyclerView.setHasFixedSize(true)
        this.taskListAdapter =
            TaskListAdapter(TaskDAOSingleton.getAllTasks(this))
                .setOnLongClickTaskListener{
                    object : DeleteTaskDialog(this) {
                        override fun onConfirm() {
                               val pos = TaskDAOSingleton.delete(baseContext, it)
                                taskListAdapter.notifyItemRemoved(pos)
                            }

                        }
                    }





        this.recyclerView.adapter = this.taskListAdapter


    }


    fun onClickOk(v: View){
        val msg: String = this.plainTextTask?.text.toString()

        var urgencia: Boolean = false
        if(msg.isNotEmpty()){
            if(switch_toggle?.isChecked){
                urgencia = true
            }
            val task = Tasks()
            //var task: Tasks = Tasks(msg, urgencia, false)
            task.setDescription(msg)
            task.setIsUrgent(urgencia)
            val pos = TaskDAOSingleton.add(baseContext, task)
            taskListAdapter.notifyItemInserted(pos)
            recyclerView.scrollToPosition(pos)
            this.plainTextTask?.setText("")

        }
    }




}