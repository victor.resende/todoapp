package br.prtt.pdm.todoapp.ui

import android.app.AlertDialog
import android.content.Context

abstract class DeleteTaskDialog(context: Context) {
    init{
        AlertDialog.Builder(context)
            .setTitle("Delete task")
            .setMessage("Are you sure you want to delete this task?")
            .setPositiveButton("Yes"){_,_ ->
                this.onConfirm()

            }
            .setNegativeButton("No", null)
            .create().show()
    }

    abstract  fun onConfirm()


}