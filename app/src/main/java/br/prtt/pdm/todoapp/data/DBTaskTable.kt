package br.prtt.pdm.todoapp.data

import android.content.ContentValues
import android.content.Context
import android.util.Log
import br.prtt.pdm.todoapp.model.Tasks

class DBTaskTable(context: Context) {
    companion object{
        private const val CH_WHERECLAUSE_ID = "${DBSchema.TaskTable.ID} = ?"
        private const val CH_ORDERBY = "${DBSchema.TaskTable.TIMESTAMP} DESC"
        private val CH_COLS = arrayOf(
            DBSchema.TaskTable.ID,
            DBSchema.TaskTable.DESCRIPTION,
            DBSchema.TaskTable.URGENT,
            DBSchema.TaskTable.DONE
        )

    }
    private val dbHelper = DBHelper(context)



    fun insert(task: Tasks): Int{
        val cv = ContentValues();
        cv.put(DBSchema.TaskTable.DESCRIPTION, task.getDescription());
        cv.put(DBSchema.TaskTable.DONE,task.getIsDone());
        cv.put(DBSchema.TaskTable.URGENT, task.getIsUrgent());
        val db =  this.dbHelper.writableDatabase
        val id = db.insert(DBSchema.TaskTable.TABLENAME, null, cv)
        Log.d("xxxx", "INSERIDO COM SUCESSO")
        db.close()
        return id.toInt()

    }

    fun delete(task: Tasks){
        val db = this.dbHelper.writableDatabase
        db.delete(DBSchema.TaskTable.TABLENAME, CH_WHERECLAUSE_ID, arrayOf(task.getId().toString()))
        db.close()


    }

    fun updateIsDone(task: Tasks){
        val cv = ContentValues()
        cv.put(DBSchema.TaskTable.DONE, task.getIsDone())
        val db = this.dbHelper.writableDatabase
        db.update(DBSchema.TaskTable.TABLENAME, cv, CH_WHERECLAUSE_ID, arrayOf(task.getId().toString()))
        db.close()

    }

    fun getAllTasks(): ArrayList<Tasks>{
        Log.d("TEST", "entrou aqui")
        val tasks = ArrayList<Tasks>()
        val db = this.dbHelper.readableDatabase
        val cur = db.query(
            DBSchema.TaskTable.TABLENAME,
            CH_COLS,
            null,
            null,
            null,
            null,
            CH_ORDERBY
        )
        while(cur.moveToNext()) {
            val id = cur.getInt(cur.getColumnIndex(DBSchema.TaskTable.ID))
            val description = cur.getString(cur.getColumnIndex(DBSchema.TaskTable.DESCRIPTION))
            val done = cur.getInt(cur.getColumnIndex(DBSchema.TaskTable.DONE))
            val doneOutput = done == 1
            val urgent = cur.getInt(cur.getColumnIndex(DBSchema.TaskTable.URGENT))
            val urgentOutput = urgent == 1
            val task = Tasks(id, description, urgentOutput, doneOutput)
            tasks.add(task)
        }
        cur.close()
        db.close()
        return tasks


    }

}